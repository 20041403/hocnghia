<!DOCTYPE html>
<html>
<head>
	<title>Tính chu vi hình chữ nhật</title>
</head>
<body>
	<?php
	$dai='';
	$rong='';
	if (ISSET($_POST['dai'])&&ISSET($_POST['rong'])){
		$dai = $_POST['dai'];
		$rong = $_POST['rong'];
		if ($dai<0||$rong<0) {
			echo 'Bạn đã nhập sai chiều dài hoặc chiều rộng hãy nhập lại!';
		}else {
			if($dai>$rong){
				echo 'Chu vi hình chữ nhật là: '.(($dai+$rong)*2);
			}else{
				echo 'Chiều dài > chiều rộng mời bạn nhập lại!';
			}
		}
	}else{
		echo 'Vui lòng nhập chiều dài và chiều rộng của hình chữ nhật';
	}
//	echo $_GET['dai']; 
	?>
	<hr>
	<form action="./chuvihcn.php" method="POST">
		<input type="number" name="dai" placeholder="Chiều dài của hình chữ nhật" value="<?php echo $dai; ?>">
		<input type="number" name="rong" placeholder="Chiều rộng của hình chữ nhật" value="<?php echo $rong; ?>" style="margin-top: 15px;">
		<button type="submit">Gửi</button>
	</form>
</body>
</html>